package ru.t1.dkozoriz.tm.model.business;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.enumerated.Status;

@NoArgsConstructor
public final class Project extends BusinessModel implements IWBS {

    public Project(@NotNull final String name) {
        super(name);
    }

    public Project(@NotNull final String name, @NotNull final Status status) {
        super(name, status);
    }

}