package ru.t1.dkozoriz.tm.api.endpoint.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.IEndpointClient;
import ru.t1.dkozoriz.tm.dto.request.task.*;
import ru.t1.dkozoriz.tm.dto.response.task.*;

public interface ITaskClient extends IEndpointClient {

    @NotNull
    ShowListTaskResponse list(@NotNull TaskShowListRequest request);

    @NotNull
    BindTaskToProjectResponse taskBindToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    UnbindTaskToProjectResponse taskUnbindToProject(@NotNull TaskUnbindToProjectRequest request);

    @NotNull
    ShowAllTasksByProjectIdResponse taskShowAllByProjectId(@NotNull TaskShowAllByProjectIdRequest request);

    @NotNull
    ChangeTaskStatusByIdResponse taskChangeStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    ChangeTaskStatusByIndexResponse taskChangeStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    CompleteTaskByIdResponse taskCompleteById(@NotNull TaskCompleteByIdRequest request);

    @NotNull
    CompleteTaskByIndexResponse taskCompleteByIndex(@NotNull TaskCompleteByIndexRequest request);

    @NotNull
    StartTaskByIdResponse taskStartById(@NotNull TaskStartByIdRequest request);

    @NotNull
    StartTaskByIndexResponse taskStartByIndex(@NotNull TaskStartByIndexRequest request);

    @NotNull
    CreateTaskResponse taskCreate(@NotNull TaskCreateRequest request);

    @NotNull
    ListClearTaskResponse taskListClear(@NotNull TaskListClearRequest request);

    @NotNull
    RemoveTaskByIdResponse taskRemoveById(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    RemoveTaskByIndexResponse taskRemoveByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull
    ShowTaskByIdResponse taskShowById(@NotNull TaskShowByIdRequest request);

    @NotNull
    ShowTaskByIndexResponse taskShowByIndex(@NotNull TaskShowByIndexRequest request);

    @NotNull
    UpdateTaskByIdResponse taskUpdateById(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    UpdateTaskByIndexResponse taskUpdateByIndex(@NotNull TaskUpdateByIndexRequest request);

}