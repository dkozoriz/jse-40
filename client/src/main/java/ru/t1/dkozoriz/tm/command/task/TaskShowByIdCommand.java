package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.task.TaskShowByIdRequest;
import ru.t1.dkozoriz.tm.model.business.Task;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    public TaskShowByIdCommand() {
        super("task-show-by-id", "show task by id.");
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final Task task =
                getEndpointLocator().getTaskEndpoint().taskShowById(new TaskShowByIdRequest(getToken(), id))
                        .getTask();
        showTask(task);
    }

}