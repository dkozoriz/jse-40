package ru.t1.dkozoriz.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.ITaskRepository;
import ru.t1.dkozoriz.tm.api.service.IConnectionService;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.ITaskService;
import ru.t1.dkozoriz.tm.comparator.CreatedComparator;
import ru.t1.dkozoriz.tm.comparator.NameComparator;
import ru.t1.dkozoriz.tm.comparator.StatusComparator;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.*;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    private final static String NAME = "Task";

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private String getName() {
        return NAME;
    }

    public TaskService(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    private IConnectionService getConnectionService() {
        return serviceLocator.getConnectionService();
    }

    private void add(@NotNull final Task task) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            taskRepository.add(task);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    private void update(@NotNull final Task task) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            taskRepository.update(task);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    private void remove(@NotNull final String userId, @NotNull final Task task) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            taskRepository.removeWithUserId(userId, task);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        add(task);
        return task;
    }

    @NotNull
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        task.setProjectId(projectId);
        add(task);
        return task;
    }

    @NotNull
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            @Nullable final List<Task> taskList = taskRepository.findAllByProjectId(userId, projectId);
            if (taskList == null) return Collections.emptyList();
            return taskList;
        }
    }

    @Override
    public int getSize(@Nullable final String userId) {
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            return taskRepository.getSizeWithUserId(userId);
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            @Nullable final List<Task> taskList = taskRepository.findAll(userId);
            if (taskList == null) return Collections.emptyList();
            return taskList;
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            @Nullable List<Task> taskList = null;
            if (sort.getComparator() == CreatedComparator.INSTANCE)
                taskList = taskRepository.findAllOrderByCreated(userId);
            if (sort.getComparator() == NameComparator.INSTANCE)
                taskList = taskRepository.findAllOrderByName(userId);
            if (sort.getComparator() == StatusComparator.INSTANCE)
                taskList = taskRepository.findAllOrderByStatus(userId);
            if (taskList != null) return taskList;
            return findAll(userId);
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        if (comparator == null) return findAll(userId);
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            @Nullable List<Task> taskList = null;
            if (comparator == CreatedComparator.INSTANCE) taskList = taskRepository.findAllOrderByCreated(userId);
            if (comparator == NameComparator.INSTANCE) taskList = taskRepository.findAllOrderByName(userId);
            if (comparator == StatusComparator.INSTANCE) taskList = taskRepository.findAllOrderByStatus(userId);
            if (taskList != null) return taskList;
            return findAll(userId);
        }
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            return taskRepository.findByIdWithUserId(userId, id);
        }
    }

    @Nullable
    @Override
    public Task findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            return taskRepository.findByIndexWithUserId(userId, index);
        }
    }

    @Override
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Task task = findById(userId, id);
        if (task == null) throw new EntityException(getName());
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

    @Override
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Task task = findByIndex(userId, index);
        if (task == null) throw new EntityException(getName());
        task.setName(name.trim());
        task.setDescription(description);
        update(task);
        return task;
    }

    @Override
    public Task changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findById(userId, id.trim());
        if (task == null) throw new EntityException(getName());
        if (status != null) task.setStatus(status);
        update(task);
        return task;
    }

    @Override
    public Task changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Task task = findByIndex(userId.trim(), index);
        if (task == null) throw new EntityException(getName());
        if (status != null) task.setStatus(status);
        update(task);
        return task;
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findById(userId, id);
        if (task == null) throw new EntityException(getName());
        remove(userId, task);
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Task task = findByIndex(userId, index);
        if (task == null) throw new EntityException(getName());
        remove(userId, task);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            taskRepository.clearWithUserId(userId);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        if (project == null) throw new EntityException("Project");
        @Nullable final Task task = serviceLocator.getTaskService().findById(userId, taskId);
        if (task == null) throw new EntityException(getName());
        task.setProjectId(projectId);
        update(task);
        return task;
    }

    @NotNull
    public Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        if (project == null) throw new EntityException("Project");
        @Nullable final Task task = serviceLocator.getTaskService().findById(userId, taskId);
        if (task == null) throw new EntityException(getName());
        task.setProjectId(null);
        update(task);
        return task;
    }

}