package ru.t1.dkozoriz.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.IProjectRepository;
import ru.t1.dkozoriz.tm.api.service.IConnectionService;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.IProjectService;
import ru.t1.dkozoriz.tm.comparator.CreatedComparator;
import ru.t1.dkozoriz.tm.comparator.NameComparator;
import ru.t1.dkozoriz.tm.comparator.StatusComparator;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.*;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class ProjectService implements IProjectService {

    private final static String NAME = "Project";

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private String getName() {
        return NAME;
    }

    public ProjectService(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    private IConnectionService getConnectionService() {
        return serviceLocator.getConnectionService();
    }

    private void add(@NotNull final Project project) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            projectRepository.add(project);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    private void update(@NotNull final Project project) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            projectRepository.update(project);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    private void remove(@NotNull final String userId, @NotNull final Project project) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            projectRepository.removeWithUserId(userId, project);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
       add(project);
        return project;
    }

    @Override
    public int getSize(@Nullable final String userId) {
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            return projectRepository.getSizeWithUserId(userId);
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @Nullable final List<Project> projectList = projectRepository.findAll(userId);
            if (projectList == null) return Collections.emptyList();
            return projectList;
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @Nullable List<Project> projectList = null;
            if (sort.getComparator() == CreatedComparator.INSTANCE)
                projectList = projectRepository.findAllOrderByCreated(userId);
            if (sort.getComparator() == NameComparator.INSTANCE)
                projectList = projectRepository.findAllOrderByName(userId);
            if (sort.getComparator() == StatusComparator.INSTANCE)
                projectList = projectRepository.findAllOrderByStatus(userId);
            if (projectList != null) return projectList;
            return findAll(userId);
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        if (comparator == null) return findAll(userId);
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @Nullable List<Project> projectList = null;
            if (comparator == CreatedComparator.INSTANCE) projectList = projectRepository.findAllOrderByCreated(userId);
            if (comparator == NameComparator.INSTANCE) projectList = projectRepository.findAllOrderByName(userId);
            if (comparator == StatusComparator.INSTANCE) projectList = projectRepository.findAllOrderByStatus(userId);
            if (projectList != null) return projectList;
            return findAll(userId);
        }
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            return projectRepository.findByIdWithUserId(userId, id);
        }
    }

    @Nullable
    @Override
    public Project findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            return projectRepository.findByIndexWithUserId(userId, index);
        }
    }

    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findById(userId, id);
        if (project == null) throw new EntityException(getName());
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

    @Override
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findByIndex(userId, index);
        if (project == null) throw new EntityException(getName());
        project.setName(name.trim());
        project.setDescription(description);
        update(project);
        return project;
    }

    @Override
    public Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = findById(userId, id.trim());
        if (project == null) throw new EntityException(getName());
        if (status != null) project.setStatus(status);
        update(project);
        return project;
    }

    @Override
    public Project changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Project project = findByIndex(userId.trim(), index);
        if (project == null) throw new EntityException(getName());
        if (status != null) project.setStatus(status);
        update(project);
        return project;
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = findById(userId, id);
        if (project == null) throw new EntityException(getName());
        remove(userId, project);
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Project project = findByIndex(userId, index);
        if (project == null) throw new EntityException(getName());
        remove(userId, project);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            projectRepository.clearWithUserId(userId);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final Project project = findById(userId, projectId);
        if (project == null) throw new EntityException(getName());
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAllByProjectId(userId, projectId);
        for (final Task task : tasks) serviceLocator.getTaskService().removeById(userId, task.getId());
        removeById(userId, projectId);
    }

}