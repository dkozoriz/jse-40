package ru.t1.dkozoriz.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.IProjectRepository;
import ru.t1.dkozoriz.tm.api.repository.ISessionRepository;
import ru.t1.dkozoriz.tm.api.repository.ITaskRepository;
import ru.t1.dkozoriz.tm.api.repository.IUserRepository;
import ru.t1.dkozoriz.tm.api.service.IConnectionService;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.ISessionService;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.Session;
import ru.t1.dkozoriz.tm.model.User;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;


public final class SessionService implements ISessionService {


    @NotNull
    private final IServiceLocator serviceLocator;

    public SessionService(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    private String getName() {
        return "Session";
    }

    @NotNull
    private IConnectionService getConnectionService() {
        return serviceLocator.getConnectionService();
    }

    public void add(@NotNull final Session session) {
        @NotNull final SqlSession sqlSession = getConnectionService().getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.add(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    private void update(@NotNull final Session session) {
        @NotNull final SqlSession sqlSession = getConnectionService().getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.update(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final SqlSession sqlSession = getConnectionService().getSqlSession();
        try {
            @NotNull ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final SqlSession sqlSession = getConnectionService().getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.clearWithUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<Session> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = getConnectionService().getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            @Nullable final List<Session> sessions = sessionRepository.findAll(userId);
            return sessions;
        }
    }

    @Nullable
    @Override
    public Session findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = getConnectionService().getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            @Nullable final Session session = sessionRepository.findById(id);
            return session;
        }
    }

    @Override
    public void remove(@Nullable final Session session) {
        if (session == null) return;
        @NotNull final SqlSession sqlSession = getConnectionService().getSqlSession();
        try {
            @NotNull ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.remove(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}