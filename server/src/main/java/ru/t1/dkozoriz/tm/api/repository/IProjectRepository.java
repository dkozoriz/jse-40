package ru.t1.dkozoriz.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.business.Project;

import java.util.List;

public interface IProjectRepository {

    @Insert(
            "INSERT INTO tm_project (row_id, user_id, name, description, status, created) " +
                    "VALUES (#{id}, #{userId}, #{name}, #{description},  #{status}, #{created})"
    )
    void add(@NotNull Project project);

    @Insert(
            "INSERT INTO tm_project (row_id, user_id, name, description, status, created) " +
                    "VALUES (#{id}, #{userId}, #{name}, #{description},  #{status}, #{created})"
    )
    void addWithUserId(@NotNull @Param("userId") String userId, @NotNull Project project);

    @Update(
            "UPDATE tm_project " +
                    "SET name = #{name}, description = #{description}, status = #{status} WHERE row_id = #{id}"
    )
    void update(@NotNull Project project);

    @Delete("DELETE FROM tm_project")
    void clear();

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clearWithUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE row_id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    Project findById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_project LIMIT #{index}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    Project findByIndex(@NotNull @Param("index") Integer index);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE row_id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    Project findByIdWithUserId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} LIMIT #{index}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    Project findByIndexWithUserId(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Nullable
    @Select("SELECT COUNT(*) FROM tm_project")
    int getSize();

    @Nullable
    @Select("SELECT COUNT(*) FROM tm_project WHERE user_id = #{userId}")
    Integer getSizeWithUserId(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_project WHERE row_id = #{id}")
    void remove(@NotNull Project project);

    @Delete("DELETE FROM tm_project WHERE row_id = #{p.id} AND user_id = #{userId}")
    void removeWithUserId(@NotNull @Param("userId") String userId, @NotNull @Param("p") Project project);

}